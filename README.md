# digital payment and trading bot

Crypto-trader
Cryptocurrency trading bot, makes use of the Bittrex API, https://github.com/ericsomdahl/python-bittrex  (included as file attachment)

The algorithm is a simple moving average crossover (2 minute time intervals, 5 period short term, 10 period long term) for the BTC-ETH market.

Requires more backtesting, use at your own risk.

Obtain Bittrex API keys after enabling 2-factor authentication on your account with the appropriate permissions. Input the keys into the secrets.json file.