from bittrex import *
import json
import time
import numpy as np


def main():
    with open('secrets.json') as secrets_file:
        secrets = json.load(secrets_file)
        secrets_file.close()

    bit = Bittrex(secrets['key'], secrets['secret'])

    arr = []
    market = 'BTC-ETH'
    last_buy = None

    sell_id = None
    buy_id = None

    while True:

        if sell_id is not None:
            if len(bit.get_open_orders(market)) != 0:
                bit.cancel(sell_id)
                sell_id = None
        if buy_id is not None:
            if len(bit.get_open_orders(market)) != 0:
                bit.cancel(buy_id)
                buy_id = None

        tick = bit.get_ticker(market)
        balance = [bit.get_balance('BTC')['result']['Available'], bit.get_balance('ETH')['result']['Available']]

        if tick['success']:
            last = tick['result']['Last']
            arr.append(last)

            if len(arr) > 10:
                prev_short = np.average(arr[-6:-1])
                prev_long = np.average(arr[-11:-1])
                short = np.average(arr[-5:])
                long = np.average(arr[-10:])

                if last_buy is None:
                    last_buy = last

                if prev_short < prev_long and short > long:

                    quantity = (balance[0] / last) * 0.99
                    buy_id = bit.buy_limit(market, quantity, last)['result']['uuid']
                    last_buy = last

                elif prev_short > prev_long and short < long:
                    if last > 1.003 * last_buy:
                        sell_id = bit.sell_limit(market, balance[1], last)['result']['uuid']

        if len(arr) > 100:
            del arr[0:50]
        time.sleep(120)


if __name__ == "__main__":
    main()